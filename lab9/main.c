#include <stdio.h>
#include <limits.h>
#define BASE 6
#define BASEE 10
unsigned long int BASEto6(unsigned long int a)
{
  int k = 1;
  unsigned long int a6 = 0;
  while (a)
  {
    a6 += k * (a % 6);
    k *= BASEE;
    a /= 6;
  }
  return a6;
}

unsigned long int BASEto10(unsigned long int a)
{
  int k = 1;
  unsigned long int a10 = 0;
  while (a)
  {
    a10 += k * (a % 10);
    k *= BASE;
    a /= 10;
  }
  return a10;
}
unsigned long int sum(unsigned long int a, unsigned long int b)
{
  unsigned long int a1, b1, s1, s;
  a1 = BASEto10(a);
  b1 = BASEto10(b);
  s = b1 * (b1 - a1) + b1;
  s1 = BASEto6(s);
  return s1;
}

int main(void)
{
  unsigned long int a, b, s2, b1, a1;
  scanf("%ld%ld", &a, &b);
  a1 = BASEto10(a);
  b1 = BASEto10(b);
  printf("%lu %lu\n", a1, b1);

  if (ULONG_MAX - b1 < a1)
    printf("!1\n");
  if (b1 < a1)
    printf("!2\n");
  if (ULONG_MAX / (b1) < a1)
    printf("!3\n");

  if (ULONG_MAX - b1 > a1)
  {
    s2 = sum(a, b);
    printf("%ld\n", s2);
  }

  return 0;
}