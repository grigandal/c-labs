#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <malloc.h>
#include <string.h>

typedef int *tmatrix_row;
typedef tmatrix_row *tmatrix;

tmatrix random_matrix(int dim)
{
    tmatrix m = (tmatrix)malloc(dim * sizeof(tmatrix));
    for (int i = 0; i < dim; i++)
    {
        tmatrix_row row = (tmatrix_row)malloc(dim * sizeof(tmatrix_row));
        for (int j = 0; j < dim; j++)
        {
            row[j] = rand() % 99999;
        }
        m[i] = row;
    }
    return m;
}

void print_row(tmatrix_row row, int dim)
{
    for (int i = 0; i < dim; i++)
    {
        int spacing = 100000;
        while (spacing >= row[i])
        {
            printf(" ");
            spacing /= 10;
        }
        printf("%d", row[i]);
    }
    printf("\n");
}

void print_matrix(tmatrix m, int dim)
{
    for (int i = 0; i < dim; i++)
    {
        print_row(m[i], dim);
    }
}

int dig_count(int v)
{
    int res = 1;
    if (v < 0)
    {
        res = 2;
        v *= -1;
    }
    while (v / 10 > 0)
    {
        res++;
        v /= 10;
    }
    return res;
}

int matrix_file_str_size(tmatrix m, int dim)
{
    int res = (dig_count(dim) + 1);
    for (int i = 0; i < dim; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            res += (dig_count(m[i][j]) + 1);
        }
    }
    return res;
}

char *matrix_to_file_str(tmatrix m, int dim)
{
    char *res = (char *)malloc(matrix_file_str_size(m, dim) * sizeof(char));
    char *tmp;
    sprintf(res, "%d\n", dim);
    int old_size;
    for (int i = 0; i < dim; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            tmp = (char *)malloc((dig_count(m[i][j]) + 1) * sizeof(char));
            if ((i + 1) * (j + 1) == dim * dim)
            {
                sprintf(tmp, "%d\n", m[i][j]);
            }
            else
            {
                sprintf(tmp, "%d ", m[i][j]);
            }
            res = strcat(res, tmp);
        }
    }
    free(tmp);
    return res;
}

char *substr(char *str, int start, int end)
{
    char *res = (char *)malloc((end - start) * sizeof(char));
    for (int i = start; i < end; i++)
    {
        res[i - start] = str[i];
    }
    return res;
}

char *pop_str(char *str, int count)
{
    int len = strlen(str);
    char *res = (char *)malloc((len - count) * sizeof(char));
    for (int i = count; i < len; i++)
    {
        res[i - count] = str[i];
    }
    return res;
}

int index_of(char s, char *str)
{
    for (int i = 0; i < strlen(str); i++)
    {
        if (str[i] == s)
        {
            return i;
        }
    }
    return -1;
}

tmatrix matrix_from_row_str(char *row_str, int dim)
{
    char *str = substr(row_str, 0, strlen(row_str));
    char *num;
    tmatrix m = (tmatrix)malloc(dim * sizeof(tmatrix));
    for (int i = 0; i < dim; i++)
    {
        tmatrix_row row = (tmatrix_row)malloc(dim * sizeof(tmatrix_row));
        for (int j = 0; j < dim; j++)
        {
            int index = index_of(' ', str);
            if (index != -1)
            {
                num = substr(str, 0, index);
                str = pop_str(str, index + 1);
            }
            else if (index_of('\n', str) != -1)
            {
                num = substr(str, 0, strlen(str) - 1);
            }
            else
            {
                num = substr(str, 0, strlen(str));
            }
            row[j] = atoi(num);
        }
        m[i] = row;
    }
    free(num);
    free(str);
    return m;
}

void save_matrix_to_file(char *filepath, tmatrix m, int dim)
{
    char *str = matrix_to_file_str(m, dim);
    FILE *f = fopen(filepath, "w");
    fprintf(f, "%s", str);
    fclose(f);
}

tmatrix matrix_from_file_str(char *str, int *dim_address)
{
    char *dim_str = substr(str, 0, index_of('\n', str));
    char *rest = pop_str(str, index_of('\n', str) + 1);
    int dim = atoi(dim_str);
    tmatrix res = matrix_from_row_str(rest, dim);
    free(dim_str);
    free(rest);
    *dim_address = dim;
    return res;
}

tmatrix read_matrix_from_file(const char *filename, int *dim_address)
{
    FILE *f = fopen(filename, "r");
    int size;
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
    char *str = (char *)malloc(size);
    fread(str, 1, size, f);
    fclose(f);
    return matrix_from_file_str(str, dim_address);
}

char *matrix_to_cli_str(tmatrix m, int dim)
{
    char *res = (char *)malloc((7 * dim) * sizeof(char));
    *res = '\0';
    char *tmp;
    char *elem;
    for (int i = 0; i < dim; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            elem = (char *)malloc(6 * sizeof(char));
            *elem = '\0';
            int spacecount = 0;
            int spacing = 100000;
            while (spacing > m[i][j])
            {
                elem = strcat(elem, " ");
                spacing /= 10;
            }
            tmp = (char *)malloc(dig_count(m[i][j]) * sizeof(char));
            *tmp = '\0';
            sprintf(tmp, "%d", m[i][j]);
            elem = strcat(elem, tmp);
            res = strcat(res, elem);
        }
        res = strcat(res, "\n");
    }
    free(tmp);
    free(elem);
    return res;
}

tmatrix transpose(tmatrix m, int dim)
{
    tmatrix tm = (tmatrix)malloc(dim * sizeof(tmatrix));
    for (int i = 0; i < dim; i++)
    {
        tmatrix_row row = (tmatrix_row)malloc(dim * sizeof(tmatrix_row));
        for (int j = 0; j < dim; j++)
        {
            row[j] = m[j][i];
        }
        tm[i] = row;
    }
    return tm;
}

tmatrix sum_matrix(tmatrix m1, tmatrix m2, int dim)
{
    tmatrix tm = (tmatrix)malloc(dim * sizeof(tmatrix));
    for (int i = 0; i < dim; i++)
    {
        tmatrix_row row = (tmatrix_row)malloc(dim * sizeof(tmatrix_row));
        for (int j = 0; j < dim; j++)
        {
            row[j] = m1[i][j] + m2[i][j];
        }
        tm[i] = row;
    }
    return tm;
}

int mul_matrix_cell(tmatrix m1, tmatrix m2, int i, int j, int dim)
{
    int res = 0;
    for (int k = 0; k < dim; k++)
    {
        res += m1[i][k] * m2[k][j];
    }
    return res;
}

tmatrix mul_matrix(tmatrix m1, tmatrix m2, int dim)
{
    tmatrix tm = (tmatrix)malloc(dim * sizeof(tmatrix));
    for (int i = 0; i < dim; i++)
    {
        tmatrix_row row = (tmatrix_row)malloc(dim * sizeof(tmatrix_row));
        for (int j = 0; j < dim; j++)
        {
            row[j] = mul_matrix_cell(m1, m2, i, j, dim);
        }
        tm[i] = row;
    }
    return tm;
}

int main(int argc, char **argv)
{
    int dim = 3;
    char *row = (char *)malloc(10000 * sizeof(char));
    scanf("%d\n", &dim);
    fgets(row, 10000, stdin);
    tmatrix matrix = matrix_from_row_str(row, dim);
    printf("%s\n", matrix_to_cli_str(matrix, dim));

    tmatrix transposed_matrix = transpose(matrix, dim);
    tmatrix multiplied_matrix = mul_matrix(matrix, transposed_matrix, dim);
    tmatrix result_matrix = sum_matrix(multiplied_matrix, matrix, dim);

    printf("%s", matrix_to_cli_str(result_matrix, dim));
    
    // save_matrix_to_file("tmp.txt", result_matrix, dim);
    // int test_dim;
    // tmatrix test_matrix = read_matrix_from_file("tmp.txt", &test_dim);
    // printf("=======================\n\n%d\n\n", test_dim);
    // printf("%s\n", matrix_to_cli_str(test_matrix, test_dim));
}