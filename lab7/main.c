#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

double calc_average(int *array, int size, int *count, int limit) // Подсчет среднего с использованием индекса
{
    int sum = 0;
    int new_size = 0;
    for (int i = 0; i < size; i++)
    {
        if (array[i] > limit)
        {
            sum += array[i];
            new_size++;
        }
    }
    *count = new_size;
    if (sum == 0)
    {
        return 0;
    }
    return (double)sum / new_size;
}

double calc_average_address(int *array, int size, int *count, int limit) // Подсчет среднего с использованием адресной арифметики
{
    int sum = 0;
    int new_size = 0;
    for (int i = 0; i < size; i++)
    {
        if (*(array + i) > limit)
        {
            sum += *(array + i);
            new_size++;
        }
    }
    *count = new_size;
    if (sum == 0)
    {
        return 0;
    }
    return (double)sum / new_size;
}

int main()
{
    int min, max;
    int array[40];
    int a;
    // scanf("%d", &min);
    // scanf("%d", &max);
    srand(time(NULL));
    for (int i = 0; i < 40; i++)
    {
        // array[i] = (rand() % (min - max + 1)) + min; // Заполнение случайным числом

        scanf("%d", &array[i]); // Заполнение вводимым числом
    }
    scanf("%d", &a);

    int *count = (int *)malloc(sizeof(int));
    double average = calc_average(&array, 40, count, a);

    printf("%.4lf\n%d", average, *count);

    return 0;
}