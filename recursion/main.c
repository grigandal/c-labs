
#include <stdio.h>
#include <stdlib.h>
int p;
long int F(int k)
{
    long int f;
    if (k <= 2)
    {
        printf("F(%d) 1 %d\n", k, p - k);
        return 1;
    }

    else
    {
        f = F(k - 1) + F(k - 2);
        printf("F(%d) %d %d\n", k, f, p - k);
        // printf("%d  F(%d)=%ld\n", p-k, k, f);
        return f;
    }
}
int main()
{
    int k;
    scanf("%d", &k);
    p = k;
    F(k);
    return 0;
}